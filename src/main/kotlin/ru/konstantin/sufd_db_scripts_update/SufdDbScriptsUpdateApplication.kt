package ru.konstantin.sufd_db_scripts_update

import kotlinx.coroutines.runBlocking
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Component
import java.io.File
import java.nio.file.Files
import java.nio.file.Path
import java.sql.Date
import java.sql.ResultSet
import kotlin.streams.toList


data class Version(val fv: String?, val cv: String?, val ad: Date?, val sp: String?)

@SpringBootApplication
class SufdDbScriptsUpdateApplication

fun main(args: Array<String>) {
    val context = runApplication<SufdDbScriptsUpdateApplication>(*args)
    val er = context.getBean("SUFD_Updater", SufdUpdater::class.java)
    er.runMe()
}

@Component("SUFD_Updater")
class SufdUpdater {
    var logger: Logger = LoggerFactory.getLogger(SufdUpdater::class.java)

    @Autowired
    lateinit var jdbcTemplate: JdbcTemplate

    @Value("\${spring.datasource.url}")
    val url = ""

    @Value("\${spring.datasource.username}")
    val login = ""

    @Value("\${spring.datasource.password}")
    val password: String = ""

    @Value("\${otr.sufd.sqlscripts.folderpath}")
    val folderPath: String = ""

    fun runMe() = runBlocking {
        logger.info("$url - $login - $password")
        //получаем список файлов со скриптами
        val fileList = getAllFilesInResources(folderPath)
        val rowMapper: RowMapper<Version> = RowMapper<Version> { resultSet: ResultSet, rowIndex: Int ->
            Version(
                resultSet.getString("FUNC_VERSION"),
                resultSet.getString("CORE_VERSION"),
                resultSet.getDate("UPDATE_DATE"), resultSet.getString("SP")
            )
        }

        //Получаем все записи из базы
        val results = jdbcTemplate.query("SELECT * FROM VERSIONS ORDER BY 3 desc", rowMapper)
        //берем максимальную версию скрипта из базы и сравниваем с версийе файлов, отбрасываем ненужные файлы
        logger.info("My version is - ${results.first()}")

        val installedScripts = mutableListOf<String>()
        //проверяем какие скрипты установленны
        for (ver in results) {
            fileList.forEach {
                var verFromFilename = it.fileName.toString().substringBeforeLast(".").substringAfter(".")
                verFromFilename = "${verFromFilename.substringBeforeLast(".")}.${verFromFilename.substringAfterLast(".").toInt()}"
                if (ver.fv == verFromFilename) {
                    installedScripts.add(it.toString())
                }
            }
        }

        //Из полного списка файлов вычитаем список установленных файлов, получаем файлы которые надо установить.
        val neededScripts = fileList.map { it.toString() } - installedScripts
        neededScripts.forEach { logger.info(it) }
        if (neededScripts.isEmpty()) {
            logger.info("У вас установлены все скрипты.")
            logger.info("Программа закончила работу...")
            return@runBlocking
        }
        logger.info("У вас не установленны следующие скрипты(см.список выше)^^^:")
        logger.info("Для продолжения установки скриптов введите 'y'")

        if (!readLine().equals("y", true)) {
            logger.info("Программа закончила работу...")
            return@runBlocking
        }
        val psqlHandler = PsqlHandler()
        psqlHandler.connectToDB(url, login, password)
        for (script in neededScripts) {
            val result = psqlHandler.runScript(script)
            var stopTheProgram = false
            result.forEach {
                logger.info(it)
                if (it.startsWith("Error starting at line")) {
                    stopTheProgram = true
                }
            }
            if (stopTheProgram) {
                logger.error("Ошибка в скрипте $script")
                return@runBlocking
            }
            logger.info("Good job $script")
        }
    }

    fun getAllFilesInResources(path: String): List<Path> {
        val resourcesPath = File(path).toPath()
        return Files.walk(resourcesPath)
            .filter { item -> Files.isRegularFile(item) }
            .filter { item -> item.toString().endsWith(".sql") }
            .toList()
            .sortedBy { it.fileName }
    }
}