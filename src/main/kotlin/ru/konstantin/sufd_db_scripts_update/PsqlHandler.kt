package ru.konstantin.sufd_db_scripts_update

import oracle.dbtools.db.ResultSetFormatter
import oracle.dbtools.raptor.newscriptrunner.ScriptExecutor
import oracle.dbtools.raptor.newscriptrunner.ScriptRunnerContext
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.*
import java.nio.charset.Charset
import java.nio.charset.StandardCharsets
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException


class PsqlHandler {
    var logger: Logger = LoggerFactory.getLogger(PsqlHandler::class.java)
    lateinit var  conn: Connection
    fun connectToDB(url: String, login: String, password: String) {
        conn = DriverManager.getConnection(url, login, password)
        conn.autoCommit = false

    }

    fun runScript(scriptAbsolutePath: String): List<String> {
        if (conn == null) {
            logger.info("Connect to DB by PsqlHandler().connectToDB(url: String, login: String, password: String)")
        }
        // #create sqlcl
        val sqlcl = ScriptExecutor(conn)

        // #setup the context
        val ctx = ScriptRunnerContext()

        // set the output max rows
        ResultSetFormatter.setMaxRows(10000)
        // #set the context
        sqlcl.setScriptRunnerContext(ctx)
        ctx.setBaseConnection(conn)

        // Capture the results without this it goes to STDOUT
        val bout = ByteArrayOutputStream()
        val buf = BufferedOutputStream(bout)
        sqlcl.setOut(buf)

        val input = File(scriptAbsolutePath).readText(Charset.forName("windows-1251"))
        val inputStream = ByteArrayInputStream(input.toByteArray())
        // # run a whole file
        sqlcl.setStmt(inputStream)
        sqlcl.run()
//        resultsList.forEach { logger.info(it) }
        inputStream.close()
        buf.close()
        bout.close()

        return bout.toString().replace(" force_print\n".toRegex(), "").split("\n").map { it.trim() }.filter { it.isNotBlank() }
    }
}